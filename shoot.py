import sys
from datetime import datetime
import webbrowser
import threading
import time

from PySide.QtGui import *
from PySide.QtCore import Qt
from PySide.QtCore import QRect, QSize
from cherrypy import quickstart

from oauth_verification import Authenticator, OauthServer


auth = Authenticator()
oauth_server = OauthServer()


class ServerThread(threading.Thread):
    def run(self):
        quickstart(oauth_server)


class LandingWidget(QWidget):
    def __init__(self):
        super(LandingWidget, self).__init__()

        #getting resolution of the screen
        dw = QDesktopWidget()
        width = dw.geometry().width()
        height = dw.geometry().height()

        #setting yellow color for palette
        pal = QPalette()
        pal.setColor(QPalette.Window, QColor(255, 210, 74))

        self.setGeometry(width/2 - 235, 50 , 370, 680)
        self.setPalette(pal)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.button = QPushButton("Click here to get started.", self)
        self.button.setFlat(True)
        self.button.setGeometry(0, 450, 370, 50)
        self.button.clicked.connect(self.sign_in_handler)
        self.show()

    def sign_in_handler(self):
        self.hide()
        webbrowser.open_new(auth.get_permission_url())
        server_thread = ServerThread()
        server_thread.start()
        while auth.verification_code == '':
            auth.update_verification_code()
            print("the code is ", auth.verification_code)
            #sleeping for 1.5 sec
            time.sleep(1.5)
        oauth_server.stop_server()
        auth.update_credentials()
        self.destroy()


class TransWidget(QWidget):
    def __init__(self):
        super(TransWidget, self).__init__()
        self.band = QRubberBand(QRubberBand.Rectangle, self)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowOpacity(0.3)
        self.rect = QRect()

    def start_shot(self):
        self.setFocus()
        self.showFullScreen()

    def mousePressEvent(self, event):
        super(TransWidget, self).mousePressEvent(event)
        self.origin = event.pos()
        if not self.band:
            self.band = QRubberBand(QRubberBand.Rectangle, self)
        self.band.setGeometry(QRect(self.origin, QSize()))
        self.band.show()

    def mouseMoveEvent(self, event):
        super(TransWidget, self).mouseMoveEvent(event)
        self.band.setGeometry(QRect(self.origin, event.pos()).normalized())

    def mouseReleaseEvent(self, event):
        super(TransWidget, self).mouseReleaseEvent(event)
        self.rect = self.band.geometry()
        x = self.rect.getRect()
        self.hide()
        p = QPixmap.grabWindow(QApplication.desktop().winId(), x[0], x[1], x[2], x[3])
        self.band.hide()
        filename = datetime.now().strftime('%Y-%m-%d_%H-%M-%S.jpg')
        p.save(filename, 'jpeg')
        auth.upload_file(filename)


class App:
    def __init__(self):
        self.app = QApplication(sys.argv)
        self.filename = ''
        self.trans_widget = TransWidget()
        icon = QIcon("shutter.png")
        menu = QMenu()

        #initializing actions
        full_screen_shot = menu.addAction("Capture Full Window")
        region_shot = menu.addAction("Capture a region")
        exit_action = menu.addAction("Exit")

        full_screen_shot.triggered.connect(self.take_full_screen_shot)
        region_shot.triggered.connect(self.take_region_shot)
        exit_action .triggered.connect(sys.exit)

        self.tray = QSystemTrayIcon()
        self.tray.setIcon(icon)
        self.tray.setContextMenu(menu)

#Code responsible for taking full screen shots
    def take_full_screen_shot(self):
        filename = datetime.now().strftime('%Y-%m-%d_%H-%M-%S.jpg')
        p = QPixmap.grabWindow(QApplication.desktop().winId())
        p.save(filename, 'jpeg')
        auth.upload_file(filename)

#Code responsible for taking screen shot of a region
    def take_region_shot(self):
        self.trans_widget.start_shot()

    def start(self):
        self.tray.show()
        self.app.exec_()
        sys.exit()

if __name__ == "__main__":
    app = App()

    if auth.is_already_logged_in():
        app.start()
    else:
        landing_widget = LandingWidget()
        app.start()