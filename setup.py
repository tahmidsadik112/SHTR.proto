import sys

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

options = {
    'build_exe': {
        'includes': 'atexit',
        'include_files': 'cert/'
    } 
}

setup(name='SHTR.proto',
      version='0.0.1',
      description='SHTR.prototype 1 . Expect lots of bugs. :(',
      options=options,
      requires=['PySide', 'google-api-python-client', 'cherrypy']
      )
