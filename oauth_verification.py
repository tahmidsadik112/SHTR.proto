import httplib2
from googleapiclient.discovery import build, MediaFileUpload
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
import cherrypy

CODE = ''


class OauthServer(object):
    def stop_server(self):
        cherrypy.engine.exit()

    @cherrypy.expose()
    def index(self, code=''):
        global CODE
        CODE = code
        return '<center><h1>You are ready to take screenshots</h1></center>'


class Authenticator:
    def __init__(self):
        self.verification_code = CODE
        self.path_to_client_secret = 'client_secret.json'
        self.client_id = '1015702721813-davr8fgfgji5eih3ab52qnke423n53it.apps.googleusercontent.com'
        self.client_secret = '1015702721813-davr8fgfgji5eih3ab52qnke423n53it.apps.googleusercontent.com'
        self.token_file_name = 'token.json'
        self.redirect_uri = 'http://localhost:8080'
        self.storage = Storage(self.token_file_name)
        self.auth_scope = 'https://www.googleapis.com/auth/drive.file'
        self.flow = flow_from_clientsecrets(self.path_to_client_secret, self.auth_scope, self.redirect_uri )
        self.credentials = self.storage.get()

    def is_already_logged_in(self):
        if self.credentials is not None:
            return True
        return False

    def get_permission_url(self):
        return self.flow.step1_get_authorize_url()

    #oauth_step_2
    def update_credentials(self):
        self.credentials = self.flow.step2_exchange(self.verification_code)
        self.storage.put(self.credentials)

    def get_drive_service(self):
        http = httplib2.Http()
        http = self.credentials.authorize(http)
        return build('drive', 'v2', http)

    def update_verification_code(self):
        self.verification_code = CODE

    def upload_file(self, fn):
        media_body = MediaFileUpload(fn, mimetype='image/jpeg', resumable=True)
        body = {
            'title': fn,
            'description': 'Screen shot taken by QuickShot',
            'mimeType': 'image/jpeg'
        }
        drive_service = self.get_drive_service()
        drive_service.files().insert(body=body, media_body=media_body).execute()